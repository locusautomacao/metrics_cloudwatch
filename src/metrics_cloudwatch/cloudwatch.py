# -*- coding: utf-8 -*-
"""AWS Cloudwatch Library.

Este módulo incorpora funcionalidades referentes à plotagem de gráficos
no CloudWatch da Amazon.

Exemplo Básico de Utilização:
    Para a construção ou o preenchimento de um gráfico simples temos que 
    instanciar a classe Cloudwatch e chamar o método postar_metrica passando como 
    parâmetros o nome da métrica, o valor da métrica e a unidade de medida::

        $ c = Cloudwatch()
        $ c.postar_metrica("ATRASO_LIVE", 1.7, 'Seconds')

    Caso não exista, a métrica será automaticamente criada na Amazon.
    O namespace é por padrão o 'IBEX/METRICS', mas também pode ser informado
    como parâmetro opcional do método postar_metrica::

        $ c.postar_metrica("ATRASO_LIVE", 2.3, 'Seconds', 'IBEX/METRICS')

Modo Debug:
    Para ativar o modo debug, que exibe uma quantidade maior de informações no 
    console, basta que ao instanciar a classe Cloudwatch seja passado como parâmetro 
    o valor True em Debug, além da região que por padrão é 'us-west-2'.

        $ c = Cloudwatch('us-west-2', True)

    Isto é o bastante para que sejam informadas no console output informações como 
    o nome e valor da métrica informada, o tempo necessário para enviar os dados à Amazon
    e se a chamada foi realizada com sucesso ou falha.

        # INFO:Cloudwatch:ATRASO_LIVE
        # INFO:Cloudwatch:1.7
        # INFO:Cloudwatch:1.0419306755065918
        # INFO:Cloudwatch:SUCCESS

Para mais informações sobre conceitos do Amazon Cloudwatch visite::

    https://docs.aws.amazon.com/pt_br/AmazonCloudWatch/latest/monitoring/cloudwatch_concepts.html#Dimension

Para mais informações sobre como publicar métricas personalizadas visite::

    https://docs.aws.amazon.com/pt_br/AmazonCloudWatch/latest/monitoring/publishingMetrics.html

Todo:
    * Capturar corretamente o instance id via método get_instance_id()

.. _Google Python Style Guide:
   http://google.github.io/styleguide/pyguide.html

"""


import boto3
import logging
import os
import datetime
import time
import threading


class Cloudwatch:
    instance_id = None
    region = None

    def __init__(self, region='us-west-2', debug_mode=False):
        try:
            self.log = None
            self.region = region
            self.debug_mode = debug_mode
            if not self.log:
                logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
                self.log = logging.getLogger('Cloudwatch')
                # self.log = logging.getLogger(__name__)
            if not self.instance_id:
                self.instance_id = self.get_instance_id()
        except Exception as e:
            if self.debug_mode:
                self.log.error(e)

    def get_instance_id(self):
        """Método responsável pela captura do ID da instância.

        Método temporariamente comentado e hardcoded por estar encontrando erro ao tentar capturar o instance id.

        Returns:
            string: The ID of the instance.
        """
        # cmd='''set -o pipefail && grep instance-id /run/cloud-init/instance-data.json | head -1 | sed 's/.*\"i-/i-/g' | sed 's/\",//g\''''    
        # status, instance_id = subprocess.getstatusoutput(cmd)
        # print(status, instance_id)
        # print(ec2_metadata.instance_id)
        return 'i-0c7c97f190feb4351'

    def postar_metrica(self, metric_name, metric_value, metric_unit='None', metric_namespace='IBEX/METRICS'):
        """Método responsável por enviar dados ao Cloudwatch.

        Inicia thread que tentará enviar os dados, da métrica informada, ao Cloudwatch da Amazon.

        Args:
            metric_name (str): The name of the metric.
            metric_value (str): The value of the metric.
            metric_unit (str): The unit of the metric. Defaults to 'None'.
                Possible values for metric_unit: 
                'Seconds'|'Microseconds'|'Milliseconds'|'Bytes'|'Kilobytes'|'Megabytes'|
                'Gigabytes'|'Terabytes'|'Bits'|'Kilobits'|'Megabits'|'Gigabits'|'Terabits'|
                'Percent'|'Count'|'Bytes/Second'|'Kilobytes/Second'|'Megabytes/Second'|
                'Gigabytes/Second'|'Terabytes/Second'|'Bits/Second'|'Kilobits/Second'|
                'Megabits/Second'|'Gigabits/Second'|'Terabits/Second'|'Count/Second'|'None' 
            metric_namespace (str): The namespace of the metric. Defaults to 'IBEX/METRICS'.
        """
        try:
            x = threading.Thread(target=self.put_metric_data_thread, args=(metric_name,metric_value,metric_unit,metric_namespace))
            x.start()        
        except Exception as e:
            if self.debug_mode:
                self.log.error(e)

    def put_metric_data_thread(self, metric_name, metric_value, metric_unit='None', metric_namespace='IBEX/METRICS'):
        """Thread de comunicação com o AWS Cloudwatch.

        Reponsável pelo envio de dados de métrica ao Cloudwatch.

        Args:
            metric_name (str): The name of the metric.
            metric_value (str): The value of the metric.
            metric_unit (str): The unit of the metric. Defaults to 'None'.
                Possible values for metric_unit: 
                'Seconds'|'Microseconds'|'Milliseconds'|'Bytes'|'Kilobytes'|'Megabytes'|
                'Gigabytes'|'Terabytes'|'Bits'|'Kilobits'|'Megabits'|'Gigabits'|'Terabits'|
                'Percent'|'Count'|'Bytes/Second'|'Kilobytes/Second'|'Megabytes/Second'|
                'Gigabytes/Second'|'Terabytes/Second'|'Bits/Second'|'Kilobits/Second'|
                'Megabits/Second'|'Gigabits/Second'|'Terabits/Second'|'Count/Second'|'None' 
            metric_namespace (str): The namespace of the metric. Defaults to 'IBEX/METRICS'.

        Raises:
            Exception: 
        """
        
        begin = None

        if self.debug_mode:
            begin = time.time()
            self.log.info(metric_name)
            self.log.info(metric_value)

        try:
            client = boto3.client('cloudwatch', self.region)
            if client:
                response = client.put_metric_data(
                    MetricData=[
                        {
                            'MetricName': metric_name, # ex.: "ATRASO_LIVE"
                            'Dimensions': [
                                {
                                    'Name': 'InstanceName',
                                    'Value': self.instance_id   # ex.: 'i-0c7c97f190feb4351'
                                },
                            ],
                            'Timestamp': datetime.datetime.utcnow(),
                            'Unit': metric_unit,    # ex.: 'Seconds'
                            'Value': metric_value   # ex.: 1.7
                        },

                    ],
                    Namespace=metric_namespace  # ex.: 'IBEX/METRICS'
                )

                if self.debug_mode and response and response['ResponseMetadata']['HTTPStatusCode'] == 200:
                    self.log.info(time.time()-begin)
                    self.log.info('SUCCESS')
        except Exception as e:
            if self.debug_mode:
                self.log.error(e)
                self.log.error('FAIL')


# if __name__ == "__main__":
#     c = Cloudwatch('us-west-2', True)
#     c.postar_metrica("ATRASO_LIVE", 1, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 2, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 3, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 4, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 5, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 6, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 7, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 8, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 9, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 1.0, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 1.1, 'Seconds')    
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 1, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 2, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 3, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 4, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 5, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 6, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 7, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 8, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 9, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 1.0, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 1.1, 'Seconds')
