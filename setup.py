from setuptools import find_packages, setup

install_requires = [
    'boto3==1.12.42'
]

docs_require = [
    'sphinx>=1.4.0',
]

tests_require = [
    'bumpversion==0.5.3',
    'coverage==.4.2',
    'pytest==3.0.5',
    'pytest-cov==2.5.1',
    'pytest-django==3.1.2',

    # Linting
    'isort==4.2.5',
    'flake8==3.0.3',
    'flake8-blind-except==0.1.1',
    'flake8-debugger==1.4.0',
]

with open("README.md", "r") as readme_file:
    readme = readme_file.read()

setup(
    name='metrics_cloudwatch',
    version='0.0.2',
    description="Put metric data into AWS Cloudwatch using Python boto3",
    long_description=readme,
    url='https://bitbucket.org/locusautomacao/metrics_cloudwatch',
    author="Felipe Rodrigues",
    author_email="felipedemacedo.cin@gmail.com",
    install_requires=install_requires,
    tests_require=tests_require,
    extras_require={
        'docs': docs_require,
        'test': tests_require,
    },
    use_scm_version=True,
    entry_points={},
    package_dir={'': 'src'},
    packages=find_packages('src'),
    include_package_data=True,
    license='MIT',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Framework :: Django',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
    ],
    zip_safe=False,
)