# Cloudwatch module

Put metric data into AWS Cloudwatch using Python boto3

# Initial Setup

- Install Python 3.7
- Install pipenv

```java
pip install pipenv
```

# How to

## Include into your project

Run the following command on terminal:

```shell
pipenv install -e git+https://bitbucket.org/locusautomacao/metrics_cloudwatch#egg=metrics_cloudwatch
```

Sample output:

```shell
Installing -e git+https://bitbucket.org/locusautomacao/metrics_cloudwatch#egg=metrics_cloudwatch…
Adding metrics_cloudwatch to Pipfile's [packages]…
✔ Installation Succeeded 
Pipfile.lock (008424) out of date, updating to (a65489)…
Locking [dev-packages] dependencies…
Locking [packages] dependencies…
✔ Success! 
Updated Pipfile.lock (008424)!
Installing dependencies from Pipfile.lock (008424)…
  🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 9/9 — 00:00:02
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.
```

Alternatively you can add the following line into requirements.txt

```shell
-e git+https://bitbucket.org/locusautomacao/metrics_cloudwatch#egg=metrics_cloudwatch
```

## Use it

```python
from metrics_cloudwatch import Cloudwatch


if __name__ == "__main__":
    c = Cloudwatch('us-west-2', True)   # True stands for DEBUG MODE ON
    c.postar_metrica("ATRASO_LIVE", 1, 'Seconds')
```

Sample Output (with Debug Mode ON):
```shell
INFO:Cloudwatch:ATRASO_LIVE
INFO:Cloudwatch:1
INFO:botocore.credentials:Found credentials in shared credentials file: ~/.aws/credentials
INFO:Cloudwatch:0.9634222984313965
INFO:Cloudwatch:SUCCESS
```

## Debug

1. Create file ~/.aws/credentials

```java
[default]
aws_access_key_id = XXXXXXXXXXXXXXXXXXXX
aws_secret_access_key = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

2. Uncomment these lines on cloudwatch.py:

```python
# if __name__ == "__main__":
    # c = Cloudwatch('us-west-2', True)
    # c.postar_metrica("ATRASO_LIVE", 1, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 2, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 3, 'Seconds')
    # time.sleep(30)
    # c.postar_metrica("ATRASO_LIVE", 4, 'Seconds')
```

3. Run:

```python
pipenv install -r requirements.txt && pipenv shell
python cloudwatch.py
```

# Results

In your AWS Cloudwatch Metrics panel, there should be graphics like this:

![Screenshot_2020-04-20_12-51-30.png](./Screenshot_2020-04-20_12-51-30.png)

# Sample Output (Debug ON)

```python
INFO:Cloudwatch:ATRASO_LIVE
INFO:Cloudwatch:1
INFO:Cloudwatch:1.2284190654754639
INFO:Cloudwatch:SUCCESS
INFO:Cloudwatch:ATRASO_LIVE
INFO:Cloudwatch:2
INFO:Cloudwatch:0.8842096328735352
INFO:Cloudwatch:SUCCESS
```

# Common Problems

### Too many simultaneous requests results in errors:

```python
    c.postar_metrica("ATRASO_LIVE", 1, 'Seconds')
    c.postar_metrica("ATRASO_LIVE", 2, 'Seconds')
    c.postar_metrica("ATRASO_LIVE", 3, 'Seconds')
    c.postar_metrica("ATRASO_LIVE", 4, 'Seconds')
    c.postar_metrica("ATRASO_LIVE", 5, 'Seconds')
    c.postar_metrica("ATRASO_LIVE", 6, 'Seconds')
    c.postar_metrica("ATRASO_LIVE", 7, 'Seconds')
    c.postar_metrica("ATRASO_LIVE", 8, 'Seconds')
    c.postar_metrica("ATRASO_LIVE", 9, 'Seconds')
```

```shell
ERROR:Cloudwatch:'endpoint_resolver'
ERROR:Cloudwatch:FAIL
```

### Solution: Add time intervals between requests.

```python
    c.postar_metrica("ATRASO_LIVE", 1, 'Seconds')
    time.sleep(30)
    c.postar_metrica("ATRASO_LIVE", 2, 'Seconds')
    time.sleep(30)
    c.postar_metrica("ATRASO_LIVE", 3, 'Seconds')
    time.sleep(30)
    c.postar_metrica("ATRASO_LIVE", 4, 'Seconds')
    time.sleep(30)
    c.postar_metrica("ATRASO_LIVE", 5, 'Seconds')
    time.sleep(30)
    c.postar_metrica("ATRASO_LIVE", 6, 'Seconds')
    time.sleep(30)
    c.postar_metrica("ATRASO_LIVE", 7, 'Seconds')
    time.sleep(30)
    c.postar_metrica("ATRASO_LIVE", 8, 'Seconds')
    time.sleep(30)
    c.postar_metrica("ATRASO_LIVE", 9, 'Seconds')
    time.sleep(30)
```
